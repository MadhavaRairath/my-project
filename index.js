//const express = require('express')
//const app = express()
//const port = 3000
//
//app.get('/', (req, res) => res.send('Hello World! This is 4 tier CI/CD'))
//
//app.listen(port, () => console.log(`Example app listening on port ${port}!`))
//
//exports.closeServer = function(){
//  server.close();
//}


var express = require('express');
var app = express();
var exports = module.exports = {};

app.get('/', function(req, res){
  res.send('Hello World! This is 4 tier CI/CD test 11');
});

var server = app.listen(3000, function(){
  console.log('Magic is happening on port 3000');
});

exports.closeServer = function(){
  server.close();
};

